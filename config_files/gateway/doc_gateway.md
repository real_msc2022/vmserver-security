https://serverfault.com/questions/431593/iptables-forwarding-between-two-interface
iptable -a


root@debiangateway:~# nmap --script broadcast-dhcp-discover
Starting Nmap 7.70 ( https://nmap.org ) at 2019-11-18 15:46 CET
Pre-scan script results:
| broadcast-dhcp-discover:
|   Response 1 of 1:
|     IP Offered: 10.15.189.188
|     DHCP Message Type: DHCPOFFER
|     Server Identifier: 10.40.22.2
|     IP Address Lease Time: 5m00s
|     Subnet Mask: 255.255.0.0
|     Router: 10.15.254.254
|     Domain Name Server: 163.5.42.40, 163.5.42.41
|     Domain Name: int.ionis-it.com
|     Broadcast Address: 10.15.255.255
|     NTP Servers: 163.5.42.40, 163.5.42.41
|     Renewal Time Value: 2m30s
|_    Rebinding Time Value: 4m22s


root@debiangateway:~# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:41:d0:ef brd ff:ff:ff:ff:ff:ff
    inet 10.15.190.137/16 brd 10.15.255.255 scope global dynamic ens33
       valid_lft 42178sec preferred_lft 42178sec
    inet6 fe80::20c:29ff:fe41:d0ef/64 scope link
       valid_lft forever preferred_lft forever
3: ens37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:41:d0:f9 brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.1/30 brd 192.168.1.3 scope global ens37
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fe41:d0f9/64 scope link
       valid_lft forever preferred_lft forever
4: ens38: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:41:d0:03 brd ff:ff:ff:ff:ff:ff
    inet 192.168.233.1/24 brd 192.168.233.255 scope global ens38
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fe41:d003/64 scope link
       valid_lft forever preferred_lft forever
