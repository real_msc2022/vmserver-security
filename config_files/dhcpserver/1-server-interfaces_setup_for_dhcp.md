### Interfaces for a dhcp server
##### 1A - Hardware NAT network adapter added on VM settings
* Be sure that it can `ping 8.8.8.8`
* check if interface ens 33 is up `ip a`
##### 1B - install dhcp server tool
`apt install isc-dhcp-server`

##### 2A - Hardware Host-Only network adpater added on VM settings
`/etc/network/interfaces`
`auto ens37
allow-hotplug ens37
iface ens37 inet static
    address
    netmask`


##### 3 main interfaces types
Host Only : a limited connection for packet exchnage ony
Bridge : Like host only but public, as if the vm is another pc on the network.
NAT : Fully autonomous dhcp client and setup. It is the easiest to start with.
##### plop