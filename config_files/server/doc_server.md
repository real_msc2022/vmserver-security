ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group defaul
    link/ether 00:0c:29:a4:b3:90 brd ff:ff:ff:ff:ff:ff
    inet 10.15.190.138/16 brd 10.15.255.255 scope global dynamic ens33
       valid_lft 40711sec preferred_lft 40711sec
    inet6 fe80::20c:29ff:fea4:b390/64 scope link
       valid_lft forever preferred_lft forever
3: ens37: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group defaul
    link/ether 00:0c:29:a4:b3:9a brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.2/30 brd 192.168.1.3 scope global ens37
       valid_lft forever preferred_lft forever
    inet6 fe80::20c:29ff:fea4:b39a/64 scope link
       valid_lft forever preferred_lft forever


